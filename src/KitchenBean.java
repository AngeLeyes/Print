
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.List;

/**
 * 后厨单
 * @author ShangJunFeng
 *
 * 2017年12月14日
 */
public class KitchenBean   implements Printable{

	private String tablename;
	private String Qty;
	private String creator;
	private List<OrderItemEntity> OrderItemEntitylist;
	
	public KitchenBean(String tablename, String qty, String creator, List<OrderItemEntity> orderItemEntitylist) {
		super();
		this.tablename = tablename;
		Qty = qty;
		this.creator = creator;
		OrderItemEntitylist = orderItemEntitylist;
	}

	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
		
		Graphics2D g2 = (Graphics2D) graphics;  
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

		g2.setColor(Color.black);//设置打印颜色为黑色  
		//打印起点坐标  
		double x= pageFormat.getImageableX();  //返回与此 PageFormat相关的 Paper对象的可成像区域左上方点的 x坐标。  
		double y= pageFormat.getImageableY();  //返回与此 PageFormat相关的 Paper对象的可成像区域左上方点的 y坐标。
		//Font.PLAIN： 普通样式常量  Font.ITALIC 斜体样式常量
		//Font.BOLD 粗体样式常量。
		Font font = new Font("宋体",Font.BOLD,10); //根据指定名称、样式和磅值大小，创建一个新 Font。

		g2.setFont(font);//设置标题打印字体     
		float heigth = font.getSize2D();//获取字体的高度  
		//设置小票的标题标题  
		g2.drawString("后厨点单",(float)x+30,(float)y+heigth); 
		
		//第二行
		float line = 2*heigth; //下一行开始打印的高度
		g2.setFont(new Font("黑体", Font.BOLD,9));//设置正文字体  
		heigth = font.getSize2D();// 字体高度  
		line+=2;
		
		//添加一行座号
		g2.drawString("座号 : "+tablename,(float)x+10,(float)y+line); 

		//设置操作员 
		line+=heigth;
		g2.drawString("操作员 : "+creator,(float)x+10,(float)y+line); 
		
		//第三行
		line+=heigth;
		g2.drawString("开单时间 : ",(float)x+10,(float)y+line);
		
		//就餐人数行
		line+=heigth;
		g2.drawString("就餐人数 : "+Qty,(float)x+10,(float)y+line);
		
		//一行虚线
		line+=heigth;
		g2.setStroke(new BasicStroke(1f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,4.0f,new float[]{4.0f},0.0f));  
		g2.drawLine((int)x,(int)(y+line),(int)x+208,(int)(y+line));  
		
		
		//品名,数量单位行
		line+=heigth+3;
		g2.setFont(new Font("黑体", Font.BOLD, 10));
		
		g2.drawString("品名",(float)x,(float)y+line);
		g2.drawString("数量单位",(float)(x+150),(float)y+line);
		
		//一行虚线
		heigth = font.getSize2D();// 字体高度  
		line+=heigth;
		g2.setStroke(new BasicStroke(1f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,4.0f,new float[]{4.0f},0.0f));  
		g2.drawLine((int)x,(int)(y+line),(int)x+208,(int)(y+line-3));  
		
		
		g2.setFont(new Font("黑体", Font.BOLD, 13));
		heigth = font.getSize2D();// 字体高度
		heigth+=4;//微调菜品间距
		int  i=1;
		//循环菜品
		for (OrderItemEntity ord : OrderItemEntitylist) {
			line+= heigth;
			
			if(ord.getRemark()!=null)
			{
				g2.drawString(i+"."+ord.getFoodName(),(float)x,(float)y+line);		
				 float size2d = font.getSize2D();
					g2.setFont(new Font("黑体", Font.BOLD, 9));
					g2.drawString("("+ord.getRemark()+")",(float)x+size2d*(ord.getFoodName().length()+3),(float)y+line);
					g2.setFont(new Font("黑体", Font.BOLD, 13));
			}
			else
			{
				//菜名
				g2.drawString(i+"."+ord.getFoodName(),(float)x,(float)y+line);
			}
			//数量
			g2.drawString((100 * (int) (double) ord.getQty() - 100 * ord.getQty() >= 0
					? String.valueOf((int) (double) ord.getQty()) + "份"
					: ord.getQty().toString() + "份"),(float)x+150,(float)y+line);
			i++;
		}
		return 0;
		
	}

	
}
