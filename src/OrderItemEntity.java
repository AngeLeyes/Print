
import java.util.Date;


/**
 * 营业单明细
 * 
 * @author ShangJunFeng
 *
 *         2017年11月6日
 */
public class OrderItemEntity implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/** 营业单明细ID */
	private Integer id;

	/** 营业单ID */
	private Integer orderId;
	
	/** 退菜数量 */
	private Double returnfoodnumber;

	/** 菜单项ID */
	private Integer menuItemId;

	/** 菜品ID */
	private Integer foodId;

	/** 菜品名称 */
	private String foodName;

	/** 数量 */
	private Double qty;

	/** 单位 */
	private String unit;

	/** 单价(单位：分） */
	private Double price;

	/** 折扣（如：789即是 7.89折) */
	private Integer discount;

	/** 额外加钱（如：不同做法，不同口味) */
	private Integer addCost;

	/** 实际金额（单位：分),最终计算的实付金额 */
	private Double amount;

	/** 是否已经打印了退菜 ,0没有,1打印了 */
	private Integer printStatus;

	/** 出品打印设备ID(0 表示没有设备） */
	private Integer printDevice;

	/** 出品打印次数 */
	private Integer printNum;

	/** 催单次数 */
	private Integer urgeNum;

	/** 最近一次打印时间 */
	private Date lastPrintTime;

	/** 状态（0 待上菜，1 已上菜，2 已取消,3 补录项) */
	private Integer status;

	/** 创建时间 */
	private Date createTime;

	/** 点菜员ID(员工ID) */
	private Integer creator;

	/*口味*/
	private String remark;

	/**
	 * 获取营业单明细ID
	 * 
	 * @return 营业单明细ID
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * 设置营业单明细ID
	 * 
	 * @param id
	 *            营业单明细ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取营业单ID
	 * 
	 * @return 营业单ID
	 */
	public Integer getOrderId() {
		return this.orderId;
	}

	/**
	 * 设置营业单ID
	 * 
	 * @param orderId
	 *            营业单ID
	 */
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	

	public Double getReturnfoodnumber() {
		return returnfoodnumber;
	}

	public void setReturnfoodnumber(Double returnfoodnumber) {
		this.returnfoodnumber = returnfoodnumber;
	}

	/**
	 * 获取菜单项ID
	 * 
	 * @return 菜单项ID
	 */
	public Integer getMenuItemId() {
		return this.menuItemId;
	}

	/**
	 * 设置菜单项ID
	 * 
	 * @param menuItemId
	 *            菜单项ID
	 */
	public void setMenuItemId(Integer menuItemId) {
		this.menuItemId = menuItemId;
	}

	/**
	 * 获取菜品ID
	 * 
	 * @return 菜品ID
	 */
	public Integer getFoodId() {
		return this.foodId;
	}

	/**
	 * 设置菜品ID
	 * 
	 * @param foodId
	 *            菜品ID
	 */
	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}

	/**
	 * 获取菜品名称
	 * 
	 * @return 菜品名称
	 */
	public String getFoodName() {
		return this.foodName;
	}

	/**
	 * 设置菜品名称
	 * 
	 * @param foodName
	 *            菜品名称
	 */
	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	/**
	 * 获取数量
	 * 
	 * @return 数量
	 */
	public Double getQty() {
		return this.qty;
	}

	/**
	 * 设置数量
	 * 
	 * @param qty
	 *            数量
	 */
	public void setQty(Double qty) {
		this.qty = qty;
	}

	/**
	 * 获取单位
	 * 
	 * @return 单位
	 */
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 设置单位
	 * 
	 * @param unit
	 *            单位
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 获取单价(单位：分）
	 * 
	 * @return 单价(单位
	 */
	public Double getPrice() {
		return this.price;
	}

	/**
	 * 设置单价(单位：分）
	 * 
	 * @param price
	 *            单价(单位
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 获取折扣（如：789即是 7.89折)
	 * 
	 * @return 折扣（如
	 */
	public Integer getDiscount() {
		return this.discount;
	}

	/**
	 * 设置折扣（如：789即是 7.89折)
	 * 
	 * @param discount
	 *            折扣（如
	 */
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	/**
	 * 获取额外加钱（如：不同做法，不同口味)
	 * 
	 * @return 额外加钱（如：不同做法
	 */
	public Integer getAddCost() {
		return this.addCost;
	}

	/**
	 * 设置额外加钱（如：不同做法，不同口味)
	 * 
	 * @param addCost
	 *            额外加钱（如：不同做法
	 */
	public void setAddCost(Integer addCost) {
		this.addCost = addCost;
	}

	/**
	 * 获取实际金额（单位：分),最终计算的实付金额
	 * 
	 * @return 实际金额（单位：分)
	 */
	public Double getAmount() {
		return this.amount;
	}

	/**
	 * 设置实际金额（单位：分),最终计算的实付金额
	 * 
	 * @param amount
	 *            实际金额（单位：分)
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * 获取是否出品打印（0 不需出品打印，1 已出品打印，2 准备出品打印)
	 * 
	 * @return 是否出品打印（0 不需出品打印
	 */
	public Integer getPrintStatus() {
		return this.printStatus;
	}

	/**
	 * 设置是否出品打印（0 不需出品打印，1 已出品打印，2 准备出品打印)
	 * 
	 * @param printStatus
	 *            是否出品打印（0 不需出品打印
	 */
	public void setPrintStatus(Integer printStatus) {
		this.printStatus = printStatus;
	}

	/**
	 * 获取出品打印设备ID(0 表示没有设备）
	 * 
	 * @return 出品打印设备ID(0 表示没有设备）
	 */
	public Integer getPrintDevice() {
		return this.printDevice;
	}

	/**
	 * 设置出品打印设备ID(0 表示没有设备）
	 * 
	 * @param printDevice
	 *            出品打印设备ID(0 表示没有设备）
	 */
	public void setPrintDevice(Integer printDevice) {
		this.printDevice = printDevice;
	}

	/**
	 * 获取出品打印次数
	 * 
	 * @return 出品打印次数
	 */
	public Integer getPrintNum() {
		return this.printNum;
	}

	/**
	 * 设置出品打印次数
	 * 
	 * @param printNum
	 *            出品打印次数
	 */
	public void setPrintNum(Integer printNum) {
		this.printNum = printNum;
	}

	/**
	 * 获取催单次数
	 * 
	 * @return 催单次数
	 */
	public Integer getUrgeNum() {
		return this.urgeNum;
	}

	/**
	 * 设置催单次数
	 * 
	 * @param urgeNum
	 *            催单次数
	 */
	public void setUrgeNum(Integer urgeNum) {
		this.urgeNum = urgeNum;
	}

	/**
	 * 获取最近一次打印时间
	 * 
	 * @return 最近一次打印时间
	 */
	public Date getLastPrintTime() {
		return this.lastPrintTime;
	}

	/**
	 * 设置最近一次打印时间
	 * 
	 * @param lastPrintTime
	 *            最近一次打印时间
	 */
	public void setLastPrintTime(Date lastPrintTime) {
		this.lastPrintTime = lastPrintTime;
	}

	/**
	 * 获取状态（0 待上菜，1 已上菜，2 已取消,3 补录项)
	 * 
	 * @return 状态（0 待上菜
	 */
	public Integer getStatus() {
		return this.status;
	}

	/**
	 * 设置状态（0 待上菜，1 已上菜，2 已取消,3 补录项)
	 * 
	 * @param status
	 *            状态（0 待上菜
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 获取创建时间
	 * 
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 设置创建时间
	 * 
	 * @param createTime
	 *            创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取点菜员ID(员工ID)
	 * 
	 * @return 点菜员ID(员工ID)
	 */
	public Integer getCreator() {
		return this.creator;
	}

	/**
	 * 设置点菜员ID(员工ID)
	 * 
	 * @param creator
	 *            点菜员ID(员工ID)
	 */
	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	/**
	 * 获取备注留言
	 * 
	 * @return 备注留言
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * 设置备注留言
	 * 
	 * @param remark
	 *            备注留言
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
}