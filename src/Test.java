
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) throws PrinterException {
		
		
		List<OrderItemEntity> arrayList = new ArrayList<>();
		OrderItemEntity orderItemEntity = new 	OrderItemEntity();
		OrderItemEntity orderItemEntity1 = new 	OrderItemEntity();
		OrderItemEntity orderItemEntity2 = new 	OrderItemEntity();
		OrderItemEntity orderItemEntity3 = new 	OrderItemEntity();
		
		orderItemEntity.setFoodName("浓香醉鸭");
		orderItemEntity.setQty((double) 3);
		orderItemEntity1.setFoodName("浓香鸭");
		orderItemEntity1.setRemark("少放盐");
		orderItemEntity1.setQty((double) 3);
		orderItemEntity2.setFoodName("西芹炒腊肉");
		orderItemEntity2.setRemark("少辣");
		orderItemEntity2.setQty((double) 3);
		orderItemEntity3.setFoodName("饭");
		orderItemEntity3.setQty((double) 3);
		
		arrayList.add(orderItemEntity);
		arrayList.add(orderItemEntity1);
		arrayList.add(orderItemEntity2);
		arrayList.add(orderItemEntity3);
		
		KitchenBean kitchenBean = new KitchenBean("一号桌", "15", "admin", arrayList);
		   Book book = new Book(); //要打印的文档
           
           //PageFormat类描述要打印的页面大小和方向  
           PageFormat pf = new PageFormat();  //初始化一个页面打印对象
           pf.setOrientation(PageFormat.PORTRAIT); //设置页面打印方向，从上往下，从左往右
         
           //设置打印纸页面信息。通过Paper设置页面的空白边距和可打印区域。必须与实际打印纸张大小相符。  
           Paper paper = new Paper(); 
           paper.setSize(297,30000);// 纸张大小  
           paper.setImageableArea(0,0,297,30000);// A4(595 X 842)设置打印区域，其实0，0应该是72，72，因为A4纸的默认X,Y边距是72  
           
           pf.setPaper(paper);  
           book.append(kitchenBean,pf);  
           
           PrinterJob job = PrinterJob.getPrinterJob();   //获取打印服务对象  
           job.setPageable(book);  //设置打印类  
           job.print();
		}
}
